# Run Fn server
fn start -d

# Start Fn Flow
./start_flow.sh

# Deploy the app comedy
fn deploy --create-app --all --local

# Run a Redis container named *kv_store*
docker run --rm -d -p 6379:6379 --name kv_store redis

# Configure REDIS_IP for the app
fn config app comedy REDIS_IP $(docker inspect --type container -f '{{.NetworkSettings.IPAddress}}' kv_store)

# Configure function_id <-> name mapping
./self_configure.sh comedy

# Invoke the app
fn invoke comedy as_you_like_it
