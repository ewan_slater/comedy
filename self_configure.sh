[ $# -eq 0 ] && { echo "Usage: $0 app-name"; exit 1; }
app_name=$1
flowserver_ip=$(docker inspect --type container -f '{{.NetworkSettings.IPAddress}}' flowserver)
fn config app $app_name COMPLETER_BASE_URL "http://$flowserver_ip:8081"
file="fn_cmd.sh"
if [ -f $file ] ; then
  rm $file
fi
fn list functions $app_name | grep -v ID | awk -v app_name="$app_name" '{print "fn config app " app_name " " toupper($1) "_ID " $3}' > $file
source $file
rm $file
