require 'fdk'
require './helper'

def move_character(context:, input:)
  STDERR.puts(":move_character, input: #{input}")
  name =  input['name'] if input.has_key?('name')
  location = {location: input['location']} if input.has_key?('location')
  if name && location
    {response: update_character(name: name, update: location)}
  else
    {response: "Invalid input #{input}"}
  end
end

FDK.handle(target: :move_character)
