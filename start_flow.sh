#!/bin/bash

export FNSERVER_IP=$(docker inspect --type container -f '{{.NetworkSettings.IPAddress}}' fnserver)

docker run --rm -d -p 8081:8081 -e API_URL="http://$FNSERVER_IP:8080/invoke" -e LOG_LEVEL=debug -e no_proxy=$FNSERVER_IP --name flowserver fnproject/flow:latest

export FLOWSERVER_IP=$(docker inspect --type container -f '{{.NetworkSettings.IPAddress}}' flowserver)

docker run --rm -d -p 3002:3000 --name flowui -e API_URL="http://$FNSERVER_IP:8080" -e COMPLETER_BASE_URL="http://$FLOWSERVER_IP:8081" fnproject/flow:ui

