require 'fdk'
require './helper'

def love_character(context:, input:)
  STDERR.puts(":love_character, input: #{input}")
  name =  input['name'] if input.has_key?('name')
  loves = {loves: input['loves']} if input.has_key?('loves')
  if name && loves
    {response: update_character(name: name, update: loves)}
  else
    {response: "Invalid input #{input}"}
  end
end

FDK.handle(target: :love_character)
