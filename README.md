# Comedy

Provides:
- The functions to implement a Shakespeare comedy
- An example "As You Like It"

## Instructions
### Step - by - step
1. Install Fn client (see https://github.com/fnproject/cli )
2. Run Fn server

	`fn start -d`

3. Start Fn Flow

	`./start_flow.sh`

4. Deploy the app comedy _(`--create_app` creates the app if it doesn't
   already exist)_

	`fn deploy --create-app --all --local`

5. Run a Redis container named *kv_store*

	`docker run -d --rm -p 6379:6379 --name kv_store redis`

6. Configure REDIS_IP for the app

	`fn config app comedy REDIS_IP $(docker inspect --type container -f '{{.NetworkSettings.IPAddress}}' kv_store)`

7. Configure function_id to function_name mapping

	`./self_configure.sh comedy`

8. Invoke the app

	`fn invoke comedy as_you_like_it`

### In one go

1.  Run the rehearse script

	`./rehearse.sh`
