require 'fdk'
require './helper'

def marry_couple(context:, input:)
  STDERR.puts(":marry_couple, input: #{input}")
  couple = input['couple']
  if couple.uniq.size == 2
    update_character(name: couple[0], update: {spouse: couple[1]})
    update_character(name: couple[1], update: {spouse: couple[0]})
    {response: "Just Married #{couple[0]} and #{couple[1]}!"}
  else
    {response: "Vicar can't marry #{couple}.  Has to resort to sherry."}
  end
end

FDK.handle(target: :marry_couple)
