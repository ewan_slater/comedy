require 'fdk'
require './helper'

def curtain_call(context:, input:)
  STDERR.puts(":curtain_call, input: #{input}")
  state = KVStore.store.keys.map do | key |
    v = KVStore.read(key: key)
    "#{v[:name]} is #{v[:gender]}, married to #{v[:spouse]} and located at the #{v[:location]}."
  end
  STDERR.puts(":curtain_call, state: #{state}")
  {response: state}
end

FDK.handle(target: :curtain_call)
