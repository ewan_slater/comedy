require 'fdk'
require './helper'
require 'facets/hash'

def disguise_character(context:, input:)
  STDERR.puts(":disguise_character, input: #{input}")
  name = input['name']
  disguise = input['disguise'].rekey
  character = KVStore.read(key: name)
  disguise[:old_identity] = character.map {|k,v| [k,v] if disguise.key?(k)}.compact.to_h
  {response: update_character(name: name, update: disguise)}
end

FDK.handle(target: :disguise_character)
