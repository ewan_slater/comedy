require 'fdk'
require './helper'

def unmask_character(context:, input:)
  STDERR.puts(":unmask_character, input: #{input}")
  name = input['name']
  character = KVStore.read(key: name)
  {response: update_character(name: name, update: character[:old_identity], delete: :old_identity)}
end

FDK.handle(target: :unmask_character)
