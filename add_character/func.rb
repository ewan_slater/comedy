require 'fdk'
require './helper'

def add_character(context:, input:)
  STDERR.puts(":add_character, input: #{input}")
  fields = {name: input['name'],
            gender: input['gender'],
            location: input['location']}
  if fields.has_value?(nil)
    {response: "All fields must be populated"}
  else
    {response: KVStore.write(key: fields[:name], value: fields)}
  end
end

FDK.handle(target: :add_character)
