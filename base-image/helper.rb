require 'redis'
require 'facets/hash'
require 'json'

class KVStore
  def self.store
    @@store ||= Redis.new(host: ENV['REDIS_IP'])
  end

  def self.write(key:, value:)
    store.set(key, value.to_json)
  end

  def self.read(key:)
    JSON.parse(store.get(key), symbolize_names: true)
  end
end

def update_character(name:, update: nil, delete: nil)
  character = KVStore.read(key: name)
  if character
    character.update(update.rekey) if update
    character.delete(delete) if delete
    KVStore.write(key: name, value: character)
  else
    "Could not find #{name}"
  end
end
