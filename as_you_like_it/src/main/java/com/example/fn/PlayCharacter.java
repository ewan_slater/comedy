package com.example.fn;

import java.io.Serializable;

public class PlayCharacter implements Serializable {
  public String name;
  public String gender;
  public String location;
  public String loves;

  public String wibble() {
    return "Wibble";
  }

  public static PlayCharacter make(String name, String gender, String location){
    PlayCharacter pc = new PlayCharacter();
    pc.name = name;
    pc.gender = gender;
    pc.location = location;
    return pc;
  }
}
