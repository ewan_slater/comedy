package com.example.fn;

import java.io.Serializable;

public class Love implements Serializable {
  public String name;
  public String loves;

  public static Love make(PlayCharacter who, PlayCharacter loves) {
    Love l = new Love();
    l.name = who.name;
    l.loves = loves.name;
    return l;
  }
}
