package com.example.fn;

import java.io.Serializable;

public class Move implements Serializable {
  public String name;
  public String location;

  public static Move make(PlayCharacter who, String where) {
    Move m = new Move();
    m.name = who.name;
    m.location = where;
    return m;
  }
}
