package com.example.fn;

import com.fnproject.fn.api.flow.Flow;
import com.fnproject.fn.api.flow.FlowFuture;
import com.fnproject.fn.api.flow.Flows;
import com.fnproject.fn.api.FnFeature;
import com.fnproject.fn.runtime.flow.FlowFeature;
import com.fnproject.fn.api.FnConfiguration;
import com.fnproject.fn.api.RuntimeContext;
import com.fnproject.fn.api.flow.HttpResponse;
import java.io.Serializable;

@FnFeature(FlowFeature.class)
public class AsYouLikeIt implements Serializable {
  //for the avoidance of the misfunction of thy quill (aka typos)
  //locations
  private static String court = "Court", forest = "Forest";

  //sexes
  private static String male = "Male", female = "Female";

  private static RuntimeContext runtimeContext;

  //functionIdStrings
  private static String fnAddCharacter, fnLoveCharacter, fnDisguiseCharacter,
                        fnMoveCharacter, fnUnmaskCharacter, fnMarryCouple, fnCurtainCall;

  private static void setRuntimeContext(RuntimeContext aContext) {
    runtimeContext = aContext;
  }

  private static RuntimeContext getRuntimeContext() {
    return runtimeContext;
  }

  @FnConfiguration
  public static void configure(RuntimeContext ctx) {
    setRuntimeContext(ctx);
    fnAddCharacter = getFunctionId("add_character");
    fnLoveCharacter = getFunctionId("love_character");
    fnDisguiseCharacter = getFunctionId("disguise_character");
    fnMoveCharacter = getFunctionId("move_character");
    fnUnmaskCharacter = getFunctionId("unmask_character");
    fnMarryCouple = getFunctionId("marry_couple");
    fnCurtainCall = getFunctionId("curtain_call");
  }

  public static String getFunctionId(String fnName) {
    String fnIdKey = fnName.trim().toUpperCase() + "_ID";
    return getRuntimeContext().getConfigurationByKey(fnIdKey)
      .orElseThrow(() -> new RuntimeException(fnIdKey + " missing"));
  }

  public String perform() {

    PlayCharacter rosalind = PlayCharacter.make("Rosalind", female, court),
      celia = PlayCharacter.make("Celia", female, court),
      orlando = PlayCharacter.make("Orlando", male, court),
      oliver = PlayCharacter.make("Oliver", male, forest),
      phoebe = PlayCharacter.make("Phoebe", female, forest),
      silvius = PlayCharacter.make("Silvius", male, forest),
      audrey = PlayCharacter.make("Audrey", female, forest),
      touchstone = PlayCharacter.make("Touchstone", male, forest);

    Flow f = Flows.currentFlow();

    //Rosalind and Celia at court with the fool, Touchstone
    FlowFuture<HttpResponse> stageFuture = f.invokeFunction(fnAddCharacter, rosalind);
    FlowFuture<Void> vFuture = stageFuture.thenRun(() -> f.invokeFunction(fnAddCharacter, celia).get())

      .thenRun(() -> f.invokeFunction(fnAddCharacter, touchstone).get())

      //Orlando arrives at court to wrestle for his fortune (as you do)
      .thenRun(() -> f.invokeFunction(fnAddCharacter, orlando).get())

      //Orlando falls in love with Rosalind (and vice versa)
      .thenRun(() -> f.invokeFunction(fnLoveCharacter, Love.make(orlando, rosalind)).get())
      .thenRun(() -> f.invokeFunction(fnLoveCharacter, Love.make(rosalind, orlando)).get())

      //Oliver arrives at court and it all kicks off...
      .thenRun(() -> f.invokeFunction(fnAddCharacter, oliver).get())

      //Rosalind and Celia disguise themselves...
      .thenRun(() -> f.invokeFunction(fnDisguiseCharacter, FalseId.make(rosalind, "Ganymede", male)).get())
      .thenRun(() -> f.invokeFunction(fnDisguiseCharacter, FalseId.make(celia, "Aliena")).get())

      //...then head to the forest, with Touchstone...
      .thenRun(() -> f.invokeFunction(fnMoveCharacter, Move.make(rosalind, forest)).get())
      .thenRun(() -> f.invokeFunction(fnMoveCharacter, Move.make(celia, forest)).get())
      .thenRun(() -> f.invokeFunction(fnMoveCharacter, Move.make(touchstone, forest)).get())

      //...followed by Orlando...
      .thenRun(() -> f.invokeFunction(fnMoveCharacter, Move.make(orlando, forest)).get())

      //...pursued by Oliver...
      .thenRun(() -> f.invokeFunction(fnMoveCharacter, Move.make(oliver, forest)).get())

      //TL;DR some other characters appear
      .thenRun(() -> f.invokeFunction(fnAddCharacter, phoebe).get())
      .thenRun(() -> f.invokeFunction(fnAddCharacter, silvius).get())
      .thenRun(() -> f.invokeFunction(fnAddCharacter, audrey).get())

      //Rosalind and Celia take off their disguises
      .thenRun(() -> f.invokeFunction(fnUnmaskCharacter, rosalind).get())
      .thenRun(() -> f.invokeFunction(fnUnmaskCharacter, celia).get())

      //Four weddings
      .thenRun(() -> f.invokeFunction(fnMarryCouple, Marriage.make(rosalind, orlando)).get())
      .thenRun(() -> f.invokeFunction(fnMarryCouple, Marriage.make(celia, oliver)).get())
      .thenRun(() -> f.invokeFunction(fnMarryCouple, Marriage.make(phoebe, silvius)).get())
      .thenRun(() -> f.invokeFunction(fnMarryCouple, Marriage.make(audrey, touchstone)).get());

      FlowFuture<ArrayResponse> arFuture = vFuture.thenCompose(o -> f.invokeFunction(fnCurtainCall, "", ArrayResponse.class));
      ArrayResponse ar = arFuture.get();
      String retVal = new String();
      for (String al : ar.response) {
        retVal += (al + "\n");
      }
      return retVal;
  }
}
