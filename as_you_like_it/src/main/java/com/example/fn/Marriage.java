package com.example.fn;

import java.io.Serializable;

public class Marriage implements Serializable {
  public String[] couple;

  public static Marriage make(PlayCharacter player1, PlayCharacter player2) {
    Marriage m = new Marriage();
    m.couple = new String[]{player1.name, player2.name};
    return m;
  }
}
