package com.example.fn;

import java.io.Serializable;

public class FalseId implements Serializable {
  public String name;
  public Disguise disguise;

  public static class Disguise implements Serializable {
    public String name;
    public String gender;
  }

  public static FalseId make(PlayCharacter trueId, String disguiseName, String disguiseGender) {
    FalseId id = new FalseId();
    id.name = trueId.name;
    id.disguise = new Disguise();
    id.disguise.name = disguiseName;
    id.disguise.gender = disguiseGender;
    return id;
  }

  public static FalseId make(PlayCharacter trueId, String disguiseName) {
    return FalseId.make(trueId, disguiseName, trueId.gender);
  }
}
