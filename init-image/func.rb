require 'fdk'
require './helper'

def get_character(context:, input:)
  STDERR.puts(":get_character, input: #{input}")
  character = KVStore.read(key: input['name']) if input.has_key?('name')
  response = character ? character : 'No character found'
  {response: response}
end

FDK.handle(target: :get_character)
